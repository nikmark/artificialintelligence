#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <stdlib.h>
#include <cstring>
#include <string>
#include <vector>
#include <iostream>

#include <limits>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>

#include <boost/thread/mutex.hpp>

#define deg2rad(a) ((a)*M_PI/180.0)
#define BUFLEN 2000
#define penalty 1000

boost::mutex sem_iterator;
boost::mutex sem_matrix;
boost::mutex sem_update;
boost::mutex sem_dsa;
boost::mutex sem_temp;

enum
{
    GOTO_MSG = 2,
    POSE_MSG = 0,
    GOAL_REACHED_MSG = 1,
    CANCEL_MSG = 3
};

struct Position
{

    std::string tag;
    double x, y, th;

    Position()
    {
        tag = "";
        x = 0.0;
        y = 0.0;
        th = 0.0;
    }
    Position(std::string tag_, double rx_, double ry_, double rth_)
    {
        tag = tag_;
        x = rx_;
        y = ry_;
        th = rth_;
    }

    bool operator<(const Position &p) const
    {
        return atoi(tag.c_str()) < atoi(p.tag.c_str());
    }

    Position &operator=(const Position &p)
    {
        tag = p.tag;
        x = p.x;
        y = p.y;
        th = p.th;
        return *this;
    }

    bool operator==(const Position &o2)
    {
        if (tag == o2.tag && x == o2.x && y == o2.y && th == o2.th)
            return true;


        return false;
    }
};

class Matrix
{
private:
    std::vector<std::vector<int> > where;

public:

    std::vector<std::vector<int> > GetWhere() const
    {
        return where;
    }

    int GetPos(int row, int column) const
    {
        return where[row][column];
    }

    void SetPos(int row, int column, int val)
    {
        where[row][column] = val;
    }

    void Resize(int row, int column)
    {
        std::vector < std::vector<int> > tmp(row, std::vector<int>(column));
        for (size_t i = 0; i < tmp.size(); i++)
        {
            for (size_t j = 0; j < tmp[0].size(); j++)
            {
                tmp[i][j] = 0;
            }
        }
        where = tmp;
    }

    Matrix(int row = 0, int column = 0)
    {
        std::vector < std::vector<int> > tmp(row, std::vector<int>(column));
        for (size_t i = 0; i < tmp.size(); i++)
        {
            for (size_t j = 0; j < tmp[0].size(); j++)
            {
                tmp[i][j] = 0;
            }
        }
        where = tmp;
    }

    Matrix(const Matrix &matrix)
    {
        where = matrix.getWhere();
    }

    int SumColumn(int pos)
    {
        int sum = 0;
        for (size_t i = 0; i < where.size(); i++)
        {
            sum += where[i][pos];
        }
        return sum;
    }

    int SumRow(int rob)
    {
        int sum = 0;
        for (size_t i = 0; i < where[rob].size(); i++)
        {
            sum += where[rob][i];
        }
        return sum;
    }
};