# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/nicolo/groovy_workspace/sandbox/DSA/src/Starter.cpp" "/home/nicolo/groovy_workspace/sandbox/DSA/build/CMakeFiles/Starter.dir/src/Starter.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "BOOST_CB_DISABLE_DEBUG"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "/opt/ros/groovy/stacks/navigation/move_base/include"
  "/opt/ros/groovy/stacks/navigation/move_base_msgs/msg_gen/cpp/include"
  "/opt/ros/groovy/stacks/navigation/nav_core/include"
  "/opt/ros/groovy/stacks/navigation/costmap_2d/cfg/cpp"
  "/opt/ros/groovy/stacks/navigation/costmap_2d/include"
  "/opt/ros/groovy/stacks/navigation/costmap_2d/msg_gen/cpp/include"
  "/opt/ros/groovy/stacks/navigation/voxel_grid/include"
  "/opt/ros/groovy/stacks/navigation/map_server/include"
  "/opt/ros/groovy/stacks/navigation/base_local_planner/include"
  "/opt/ros/groovy/stacks/navigation/base_local_planner/cfg/cpp"
  "/opt/ros/groovy/stacks/navigation/base_local_planner/msg_gen/cpp/include"
  "/opt/ros/groovy/stacks/navigation/navfn/include"
  "/opt/ros/groovy/stacks/navigation/navfn/cfg/cpp"
  "/opt/ros/groovy/stacks/navigation/navfn/srv_gen/cpp/include"
  "/opt/ros/groovy/stacks/navigation/clear_costmap_recovery/include"
  "/opt/ros/groovy/stacks/navigation/rotate_recovery/include"
  "/home/nicolo/groovy_workspace/sandbox/TCPInterface/include"
  "/home/nicolo/groovy_workspace/sandbox/TCPInterface/msg_gen/cpp/include"
  "/opt/ros/groovy/stacks/bullet/include"
  "/opt/ros/groovy/include"
  "/opt/ros/groovy/include/pcl-1.6"
  "/usr/include/eigen3"
  "/usr/include/vtk-5.8"
  "/usr/include/qhull"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
