\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {italian}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduzione}{2}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Esecuzione del sistema}{3}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}L'algoritmo DSA}{5}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Implementazione dell'algoritmo}{7}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}ROS}{8}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Ambiente Stage}{8}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}File {\tt .launch} dei robot}{11}{subsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Nodi C++}{11}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}\tt DataStructs.h}{11}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}\tt Starter.cpp}{12}{subsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.3}\tt CoordinatorNode.cpp}{12}{subsection.4.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Il metodo {\tt CoordinatorNode::DSA()}}{13}{section*.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Il metodo {\tt CoordinatorNode::CostFunction(int n, Position now, \\ Position goal)}}{13}{section*.10}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.4}Struttura dei messaggi}{14}{subsection.4.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Conclusioni}{16}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Appendice \numberline {A}Costmap e Planner files}{17}{Appendice.1.A}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Appendice \numberline {B}Esempio di {\tt robot\_*.launch}}{18}{Appendice.1.B}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliografia}{19}{lstnumber.-8.26}
