#!/bin/bash - 
#===============================================================================
#
#          FILE:  mrputils.sh
# 
#         USAGE:  ./mrputils.sh 
# 
#   DESCRIPTION:  
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR: Luca Marchetti (luca.marchetti@cipicchia.net), 
#       COMPANY: 
#       CREATED: 11/02/2011 05:01:20 PM CET
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

pkgdir=`rospack find DSA`
pidfile=$pkgdir/run.pid
MRP_verbose=0

function mrpecho()
{
	if [ $MRP_verbose == 1 ]
	then
		echo $@
	fi
}

function mrpexec()
{
	exec $@ &
	pid=$!
	mrpecho "Getting pid of '[$@]': $pid"
	echo $pid >> $pidfile
}

function mrptexec()
{
	mrpexec ${TERM} -hold -e $@
}

function mrplaunch()
{
	mrpexec roslaunch DSA $@
}

function mrptlaunch()
{
	mrptexec roslaunch DSA $@
}

function mrprun()
{
	mrpexec rosrun DSA $@
}

function mrptrun()
{
	mrptexec rosrun DSA $@
}

function mrpkillall()
{
	if [ -f $pidfile ]
	then
		for p in $(tac $pidfile) # processes are killed in reverse order (tac)
		do
			mrpecho "Killing pid $p"
			kill -2 $p
		done

		rm -f $pidfile
	fi
}

function usage()
{
	echo "$(basename $0) [-e] [-v]"
	echo -e " e :\tEcho the fullpath of this file"
	echo -e " v :\tEnable verbose mode"
}

while getopts ":ev" opt
do
	case $opt in
		e)
			arg=$(echo $@ | sed 's@e@@g' | sed 's@- @@g' | sed 's@-$@@g')
			echo $pkgdir/scripts/mrputils.sh $arg
			;;
		v)
			MRP_verbose=1
			;;
		\?)
			usage
			;;
	esac
done


export MRP_verbose

