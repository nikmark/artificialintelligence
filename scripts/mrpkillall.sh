#!/bin/bash - 
#===============================================================================
#
#          FILE:  mrpkillall.sh
# 
#         USAGE:  ./mrpkillall.sh 
# 
#   DESCRIPTION:  
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR: YOUR NAME (), 
#       COMPANY: 
#       CREATED: 11/02/2011 04:41:27 PM CET
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

# Source the mrputils and call mrpkillall
# useful for external call of mrpkillall

#source $(rosrun settings mrputils.sh -e)
source $(rosrun DSA mrputils.sh -ev) # for verbose output on launch and kill

mrpkillall

rm `pwd`/launch/robot_*.launch
