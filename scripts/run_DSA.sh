#!/bin/bash
echo -e "\n"
echo "Running DSA test"
echo -e "\n"

source $(rosrun DSA mrputils.sh -e)
pkgdir=`rospack find DSA`
pidfile=$pkgdir/run.pid

##### DSA robot #####
echo -e "\n"
echo -e "\tExperiment: DSA, MyWorld map"
echo -e "\n"
#launching stage on the bra map with only one robot
mrptlaunch DSA.launch
sleep 5
echo `pidof tcpinterface` >> $pidfile
echo `pidof -x /opt/ros/groovy/bin/roslaunch` >> $pidfile


#launching the ros controller (navigation and localization) on the bra map
#mrptlaunch bra_1robot.launch
#sleep 10 # wait for all node to start before starting monitor

#testing if mrpdcop is doing robot initialization
#mrptrun mrpdcop -t DCOP -m cumberland_ps

#launching the coverage module that will send gotopos commands to the controller
#mrptlaunch roslaunch coverage 

