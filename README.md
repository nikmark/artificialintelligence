# DSA utilizzando nodi ROS e TCPInterface
 ==============================


Implementazione dell'algoritmo di coordinamento multi-robot DSA, utilizzando [ROS (Robot Operating System)](http://www.ros.org), implementando lo scambio di messaggi con l'interfaccia TCP di nome TCPInterface, package che ci permette di comunicare col sistema di navigazione di ROS utilizzando comandi del tipo "gotopose" o "getpose".


* Studente: Nicolò Marchi
* Matricola: VR365684
* Insegnamento: Intelligenza Artificiale
* Anno Accademico: 2012/2013
* Docente: Alessandro Farinelli

# Utilizzo 
-------------------------

Avviare dalla cartella principale del package lo script .sh presente nella cartella scripts:

    ./scripts/run_DSA.sh



Per fermare l'esecuzione utilizzare l'apposito script di kill presente nella cartella scripts:

    ./scripts/mrpkillall.sh



Per modificare le posizioni che devono essere visitate dai robot, modificare il file `PositionsSet.txt` presente nella cartella `data`.

Per modificare il numero di robot (default = 2) occorre inserire le posizioni iniziali dei nuovi robot nel file `InitPos.txt` presente sempre nella cartella `data`.


> # ENJOY!!!!
