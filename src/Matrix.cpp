#include <iostream>

class Matrix {
private:
    std::vector<std::vector<int> > where;

public:

    std::vector<std::vector<int> > getWhere() const {
        return where;
    }

    int getPos(int row, int column) const {
        return where[row][column];
    }

    void setPos(int row, int column, int val) {
        where[row][column] = val;
    }

    void resize(int row, int column) {
        std::vector < std::vector<int> > tmp(row, std::vector<int>(column));
        for (size_t i = 0; i < tmp.size(); i++) {
            for (size_t j = 0; j < tmp[0].size(); j++) {
                tmp[i][j]=0;
            }
        }
        where = tmp;
    }

    Matrix(int row = 0, int column = 0) {
        std::vector < std::vector<int> > tmp(row, std::vector<int>(column));
        for (size_t i = 0; i < tmp.size(); i++) {
            for (size_t j = 0; j < tmp[0].size(); j++) {
                tmp[i][j]=0;
            }
        }
        where = tmp;
    }

    Matrix(const Matrix &matrix) {
        where = matrix.getWhere();
    }

    int sumColumn(int pos) {
        int sum = 0;
        for (size_t i = 0; i < where.size(); i++) {
            sum += where[i][pos];
        }
        return sum;
    }

     int SumRow(int rob) {
        int sum = 0;
        for (size_t i = 0; i < where[rob].size(); i++) {
            sum += where[rob][i];
        }
        return sum;
    }
};
