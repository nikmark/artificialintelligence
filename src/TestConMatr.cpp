#include <cassert>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <vector>
#include <stdlib.h>

#include <ros/package.h>

#include <sstream>

#include <thread>

#include "DataStructs.h"
#include "coordinator.cpp"

#define deg2rad(a) ((a)*M_PI/180.0)

static char bufin[BUFLEN];
Matrix where;

int main(int argc, char **argv)
{

  ros::init(argc, argv, "Test");

  /* Retrieve the task set from a file */
  std::string DIR = ros::package::getPath("DSA");
  std::string PATH_POS = DIR + "/data/PositionsSet.txt";
  std::string PATH_INIT = DIR + "/data/InitPos.txt";

  std::ifstream fin(PATH_POS.c_str());
  std::ifstream fin_init(PATH_INIT.c_str());

  assert(fin);
  assert(fin_init);

  std::vector<Position> pos;
  std::vector<Position> init_pos;

  Position tmp("", 0.0, 0.0, 0.0);

  std::string tag;
  int cont = 0;

  std::string x_str;

  while (fin >> x_str)
  {
    sprintf(bufin, "%d", cont);
    tmp.tag = std::string(bufin);

    tmp.x = boost::lexical_cast<double>(x_str);
    fin >> tmp.y >> tmp.th;
    std::cout << "[ " << tmp.x << ", " << tmp.y << ", " << tmp.th << " ]" << " - " << tmp.tag << std::endl;
    pos.push_back(tmp);
    cont++;
    x_str.clear();
  }

  float x, y, th;
  while (fin_init >> x)
  {
    fin_init >> y;
    fin_init >> th;
    std::cout << "[ " << x << ", " << y << ", " << th << " ]" <<  " - -1" << std::endl;
    init_pos.push_back(Position("-1", x, y, th));
  }

  std::vector<CoordinatorNode> agents(init_pos.size());

  std::vector<int> ports;
  int port_tmp = 9000;

  std::cout << std::endl;

    std::cout << "init pos size = " << init_pos.size() << std::endl;

  for (size_t i = 0; i < init_pos.size(); i++)
  {
    port_tmp += (i);
    ports.push_back(port_tmp);
      std::cout << "port tmp = " << port_tmp << std::endl;
      port_tmp -= i;

  }

  std::string s;

  FILE *lsofFile_p = popen("rosversion -d", "r");

  if (!lsofFile_p)
  {
    return -1;
  }

  char buffer[1024];
  char *line_p = fgets(buffer, sizeof(buffer), lsofFile_p);
  strtok(line_p, "\n");
  pclose(lsofFile_p);


  for (size_t i = 0; i < init_pos.size(); i++)
  {
    s.append("robot_");
    s.append(boost::lexical_cast<std::string>(i));

    std::string stemp(s);
    stemp.append(".launch");
    std::string path = DIR + "/launch/" + stemp;
    std::ofstream fout(path.c_str());

    std::stringstream ss;

    ss << "<launch>" << std::endl;
    ss << "<group ns=\"" << s << "\">" << std::endl;
    ss << "<param name=\"tf_prefix\" value=\"" << s << "\" />" << std::endl;
    ss << "<rosparam param=\"initial_pos\">[" << init_pos[i].x << ", " << init_pos[i].x << "]</rosparam>" << std::endl;
    ss << "<include file=\"$(find DSA)/config/move_base.launch\" />" << std::endl;
    ss << "<include file=\"$(find DSA)/config/amcl_node.launch\" />" << std::endl;
    ss << "<param name=\"amcl/initial_pose_x\" value=\"" << init_pos[i].x << "\" />" << std::endl;
    ss << "<param name=\"amcl/initial_pose_y\" value=\"" << init_pos[i].y << "\" />" << std::endl;
    ss << "<param name=\"amcl/initial_pose_a\" value=\"" << std::setprecision(4) << deg2rad(init_pos[i].th) << "\" />" << std::endl;
    ss << "<param name=\"global_costmap/global_frame\" value=\"/" << s << "/map\" />" << std::endl;
    ss << "<param name=\"global_costmap/robot_base_frame\" value=\"/" << s << "/base_link\" />" << std::endl;
    ss << "<param name=\"local_costmap/robot_base_frame\" value=\"/" << s << "/base_link\" />" << std::endl;
    ss << "<param name=\"local_costmap/global_frame\" value=\"/" << s << "/odom\" />" << std::endl;
    ss << "<param name=\"amcl/odom_frame_id\" value=\"/" << s << "/odom\" />" << std::endl;
    ss << "<param name=\"amcl/base_frame_id\" value=\"/" << s << "/base_link\" />" << std::endl;
    ss << "<node pkg=\"tf\" type=\"static_transform_publisher\" name=\"link_broadcaster\" args=\"0 0 0 0 0 0 /map /" << s << "/map 100\" />" << std::endl;
    ss << "<!-- Run TCPInterface -->" << std::endl;
    ss << "<node pkg=\"TCPInterface\" type=\"tcpinterface\" name=\"TCPInterface\" output=\"screen\">" << std::endl;
    ss << "<param name=\"TCP_server_port\" type=\"int\" value=\"" << ports[i] << "\" />" << std::endl;
    ss << "<param name=\"config_file\" type=\"string\" value=\"$(find TCPInterface)/config/UDPpeers.cfg\" /> " << std::endl;
    ss << "</node>" << std::endl;
    ss << "</group>" << std::endl;
    ss << "</launch>" << std::endl;

    std::string cmd("gnome-terminal --geometry=80x24+900+0 -x bash -c \"/opt/ros/");
    cmd.append(line_p);
    cmd.append("/bin/roslaunch DSA ");
    cmd.append(stemp);
    cmd.append("; read \"");
    fout << ss.str();
    fout.close();
    ROS_DEBUG(cmd.c_str());

    if (system(cmd.c_str()))
    {
      ROS_ERROR("ERROR -- Call o system didn't work.");
    }
    s.clear();
  }

  boost::this_thread::sleep(boost::posix_time::seconds(8));

  s.clear();

  where.resize(init_pos.size(), pos.size());
  std::cout << where.getWhere().size() << std::endl;

  boost::thread_group threads;

  //Initialization and execution
  for (size_t i = 0; i < init_pos.size(); i++)
  {
    s.append("robot_");
    s.append(boost::lexical_cast<std::string>(i));
    std::vector<int> tmpPorts = ports;
    tmpPorts.erase(std::find(tmpPorts.begin(), tmpPorts.end(), ports[i]));

    agents[i] = CoordinatorNode(ports[i], s.c_str(), where, argc, argv, pos, init_pos[i], tmpPorts);
    boost::thread* th = new boost::thread(agents[i]);
    threads.add_thread(th);

    s.clear();
    boost::this_thread::sleep(boost::posix_time::seconds(1));

  }
	
  boost::this_thread::sleep(boost::posix_time::seconds(3));
  threads.join_all();

  while(ros::ok()){}
  //ros::spin();
}
// vaffanculo :)