#include <ros/ros.h>
#include <fstream>
#include <cassert>
#include <vector>
#include <algorithm>
#include <string>
#include <climits>
#include <sstream>

#include <TCPInterface/TcpClient.h>
#include <TCPInterface/RCOMMessageToSend.h>

#include <move_base_msgs/MoveBaseAction.h>
#include <actionlib/client/simple_action_client.h>

#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseStamped.h>

#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

#include <boost/thread.hpp>
#include <boost/random.hpp>

#include "Matrix.cpp"

class CoordinatorNode
{
    int g_sd = 0;
    struct sockaddr_in g_server_addr, g_my_addr; /* l'indirizzo del server */
    char g_buff[BUFLEN]; /* dati di invio e ricezione */

    Position g_init;
    Position g_goal;

    char g_bufin[BUFLEN], g_bufout[BUFLEN];

    std::vector<long int> cost_array;
    std::vector<int> port_others;

    int pos_prec_mine = -1, my_pos = -1, my_rob = -1;

    int port = 0;
    std::string g_robot;
    Matrix m;
    std::vector<Position> pos;

    int argc = 0;
    char **argv;

    ros::NodeHandle n;
    ros::Subscriber RCOMMessageSubscriber;
    ros::Subscriber sub;
    ros::Subscriber goal_subscriber;

    ros::Publisher goal_delete_publisher;

public:

    CoordinatorNode();
    CoordinatorNode(int, std::string, Matrix &, int, char **, std::vector<Position>, Position, std::vector<int>);

    void operator()(void)
    {

        std::string s1, s2, s3, s4;

        s1.append("coordinator_");
        s1.append(g_robot);

        ros::init(argc, argv, s1);

        s1.clear();
        s1.append("/");
        s1.append(g_robot);
        s1.append("/");

        s2 = s1;
        s3 = s1;
        s4 = s1;

        s1.append("amcl_pose");
        s2.append("RCOMmessageReceived");

        s3.append("move_base/result");
        s4.append("move_base/cancel");

        sub = n.subscribe(s1.c_str(), 1000, &CoordinatorNode::NowPoseCallback, this);
        RCOMMessageSubscriber = n.subscribe(s2.c_str(), 1000, &CoordinatorNode::RCOMMessageToSendCallback, this);
        goal_subscriber = n.subscribe(s3.c_str(), 1000, &CoordinatorNode::IsSucceded, this);
        goal_delete_publisher = n.advertise<actionlib_msgs::GoalID>(s4.c_str(), 1000);

        Connection();

        while (true)
        {
            CoordinatorNode::DSA();

            sem_dsa.lock();
            ros::spinOnce();

            bool guard = false;

            if (pos.size() == 0)
            {
                guard = true;
            }

            if (pos.size() == port_others.size())
            {

                sem_iterator.lock();
                std::vector<Position>::iterator it = pos.begin();
                sem_iterator.unlock();

                int pos_pos = boost::lexical_cast<int>((*it).tag);

                if (m.SumColumn(pos_pos) > 0 && m.GetPos(my_rob, pos_pos) == 0)
                {
                    guard = true;
                }
            }


            if (guard)
            {
                SendPose(g_goal, CANCEL_MSG, boost::lexical_cast < std::string > (port));

                ROS_INFO("Robot= %s, NO MORE POSITIONS. EXECUTION COMPLETE!!!", g_robot.c_str());
                sem_dsa.unlock();

                break;
            }

            sem_dsa.unlock();

        }
    }

private:

    bool SendPose(Position, int, std::string);

    void NowPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &);
    void RCOMMessageToSendCallback(const TCPInterface::RCOMMessageToSend::ConstPtr &);
    void IsSucceded(const move_base_msgs::MoveBaseActionResult::ConstPtr &);

    long int CostFunction(int, Position, Position);
    int GetRandomChoice(int);

    bool UpdMatrix(int, int, int);

    void Connection();

    void PrintMatrix(Matrix);

    void DSA();

};

CoordinatorNode::CoordinatorNode()
{
}

CoordinatorNode::CoordinatorNode(int _port, std::string _g_robot, Matrix &_m, int _argc, char **_argv, std::vector<Position> _pos, Position _g_init, std::vector<int> _port_others)
{
    port = _port;
    g_robot = _g_robot;
    m = _m;
    pos = _pos;
    g_init = _g_init;
    port_others = _port_others;

    argc = _argc;
    argv = _argv;

    _g_robot.erase(_g_robot.begin(), _g_robot.begin() + 6);
    my_rob = atoi(_g_robot.c_str());

    cost_array.Resize(pos.size());
    fill(cost_array.begin(), cost_array.end(), std::numeric_limits<int>::max());


}

void CoordinatorNode::Connection()
{
    struct hostent *hp;

    hp = gethostbyname("127.0.0.1");

    g_my_addr.sin_family = AF_INET;
    g_my_addr.sin_port = htons(port);
    g_my_addr.sin_addr.s_addr = ((struct in_addr *) (hp->h_addr))->s_addr;

    if ((g_sd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        ROS_INFO("Error creating the socket\n");

    if (connect(g_sd, (struct sockaddr *) &g_my_addr, sizeof(g_my_addr)) < 0)
    {
        ROS_INFO("Problem with the Connection to the server\n");
        pthread_cancel(3);
    }

    ::recv(g_sd, (char *) g_bufin, BUFLEN, 0);
}

void CoordinatorNode::DSA()
{
    sem_dsa.lock();

    ros::spinOnce();

    sem_iterator.lock();
    std::random_shuffle(pos.begin(), pos.end());
    std::vector<Position>::iterator it = pos.begin();
    sem_iterator.unlock();

    for (; it != pos.end(); it++)
    {
        ros::spinOnce();

        //choose a position
        Position temp = *it;

        //calc the cost function and fill the cost table
        my_pos = atoi(temp.tag.c_str());

        ros::spinOnce();

        long int cost = CoordinatorNode::CostFunction(m.SumColumn(my_pos), temp, g_init);

        if (cost < cost_array[my_pos] && (cost < cost_array[pos_prec_mine] || pos_prec_mine == -1))
        {

            sem_matrix.lock();
            bool test = CoordinatorNode::UpdMatrix(my_rob, pos_prec_mine, my_pos);
            sem_matrix.unlock();

            if (!test)
            {
                boost::this_thread::sleep(boost::posix_time::seconds(1));
                break;
            }

            //go to the selected position
            SendPose(temp, GOTO_MSG, boost::lexical_cast < std::string > (port));

            // if (m.SumColumn(my_pos) > 1 && m.GetPos(my_rob, my_pos) == 1)
            // {
            //  ROS_INFO("DEBUG %s -- Qualcuno sta gia andando qui", g_robot.c_str());
            //  SendPose(temp, CANCEL_MSG, boost::lexical_cast < std::string > (port));
            //  boost::this_thread::sleep(boost::posix_time::seconds(1));
            //     break;
            // }

            //send choice to other agents
            for (std::vector<int>::iterator it = port_others.begin(); it != port_others.end(); it++)
            {
                SendPose(temp, POSE_MSG, boost::lexical_cast < std::string > (*it));
            }

            g_goal = temp;
            cost_array[my_pos] = cost;
            pos_prec_mine = my_pos;

            ros::spinOnce();

        }

        ros::spinOnce();


    }

    sem_dsa.unlock();
    boost::this_thread::sleep(boost::posix_time::seconds(1));

}

void CoordinatorNode::PrintMatrix(Matrix m)
{

    std::stringstream ss;
    for (size_t i = 0; i < m.GetWhere().size(); i++)
    {

        for (size_t j = 0; j < m.GetWhere()[0].size(); j++)
        {
            std::vector<std::vector<int> > v;
            v = m.GetWhere();
            ss << v[i][j];

        }
        ss << "; ";
    }

    ROS_INFO("%s", ss.str().c_str());
}

void CoordinatorNode::NowPoseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr &msg)
{
    g_init.x = boost::numeric_cast<double>(msg->pose.pose.position.x);
    g_init.y = boost::numeric_cast<double>(msg->pose.pose.position.y);
    g_init.th = boost::numeric_cast<double>(msg->pose.pose.position.z);
}

void CoordinatorNode::RCOMMessageToSendCallback(const TCPInterface::RCOMMessageToSend::ConstPtr &msg)
{
    std::string temp = msg->value;
    std::vector<std::string> tokens;
    boost::split(tokens, temp, boost::is_any_of(" "));

    int rob = boost::lexical_cast<int>(tokens[1]);
    int pos_to_change = boost::lexical_cast<int>(tokens[3]);

    std::string command(tokens[2].c_str());

    if (command == "Remove")
    {
        sem_iterator.lock();
        std::vector<Position>::iterator it = pos.begin();
        sem_iterator.unlock();

        std::string s2(tokens[3]);

        for (; it != pos.end(); it++)
        {
            std::string s1((*it).tag);
            if (s1 == s2)
            {
                pos.erase(it);
                break;
            }
        }

        sem_matrix.lock();
        CoordinatorNode::UpdMatrix(rob, pos_to_change, -1);
        sem_matrix.unlock();
    }
    else
    {
        int pos_gone = boost::lexical_cast<int>(tokens[5]);

        sem_matrix.lock();
        CoordinatorNode::UpdMatrix(rob, pos_gone, pos_to_change);
        sem_matrix.unlock();
    }
}

bool CoordinatorNode::UpdMatrix(int my_rob, int pos_prec, int my_pos)
{
    bool guard = false;
    //PrintMatrix(m);

    if (pos_prec == -1)
    {
        m.SetPos(my_rob, my_pos, 1);
        guard = true;
    }

    if (my_pos == -1)
    {
        m.SetPos(my_rob, pos_prec, 0);
        guard = true;
    }

    if (pos_prec != -1 && my_pos != -1)
    {
        m.SetPos(my_rob, pos_prec, 0);
        m.SetPos(my_rob, my_pos, 1);
        guard = true;
    }

    //PrintMatrix(m);

    return guard;
}

bool CoordinatorNode::SendPose(Position pose, int type, std::string dest)
{
    bool ret;
    std::string send_pose("send 127.0.0.1:");
    send_pose.append(dest);
    send_pose.append(" ");
    std::string send_goto("gotopose ");
    std::string send_stop("stop ");

    switch (type)
    {
    case POSE_MSG:
    {

        send_pose.append("Robot ");
        send_pose.append(boost::lexical_cast < std::string > (my_rob));
        send_pose.append(" GoingTo ");
        send_pose.append(pose.tag);
        send_pose.append(" GoneTo ");
        send_pose.append(boost::lexical_cast<std::string>(pos_prec_mine));
        send_pose.append("\n\r");

        sprintf(g_bufout, "%s", send_pose.c_str());
        ROS_INFO("stringa pos= %s", g_bufout);

        ::send(g_sd, g_bufout, strlen(g_bufout), MSG_MORE);

        ::recv(g_sd, (char *) g_bufin, BUFLEN, 0);

        if (strcmp(g_bufin, "OK send") != 0)
            ret = false;

        ret = true;
        break;
    }

    case GOAL_REACHED_MSG:
    {

        send_pose.append("Robot ");
        send_pose.append(boost::lexical_cast < std::string > (my_rob));
        send_pose.append(" Remove ");
        send_pose.append(pose.tag.c_str());
        send_pose.append("\n\r");

        sprintf(g_bufout, "%s", send_pose.c_str());
        ROS_INFO("stringa pos= %s", g_bufout);

        ::send(g_sd, g_bufout, strlen(g_bufout), MSG_MORE);

        ::recv(g_sd, (char *) g_bufin, BUFLEN, 0);

        if (strcmp(g_bufin, "OK send") != 0)
            ret = false;

        ret = true;
        break;
    }


    case GOTO_MSG:
    {

        send_goto.append(boost::lexical_cast < std::string > (pose.x));
        send_goto.append(" ");
        send_goto.append(boost::lexical_cast < std::string > (pose.y));
        send_goto.append(" ");
        send_goto.append(boost::lexical_cast < std::string > (pose.th));
        send_goto.append("\n\r");

        sprintf(g_bufout, "%s", send_goto.c_str());
        ROS_INFO("stringa goto= %s", g_bufout);

        ::send(g_sd, g_bufout, strlen(g_bufout), MSG_MORE);

        ::recv(g_sd, (char *) g_bufin, BUFLEN, 0);

        std::string retu("OK gotopose: ");
        send_goto.erase(send_goto.begin(), send_goto.begin() + 9);
        retu.append(send_goto);
        if (strcmp(g_bufin, retu.c_str()) != 0)
            ret = false;

        ret = true;
        break;
    }

    case CANCEL_MSG:
    {

        send_stop.append("\n\r");

        sprintf(g_bufout, "%s", send_stop.c_str());
        ROS_INFO("stringa cancel= %s", g_bufout);

        ::send(g_sd, g_bufout, strlen(g_bufout), MSG_MORE);

        ::recv(g_sd, (char *) g_bufin, BUFLEN, 0);

        std::string reu("OK stop");
        if (strcmp(g_bufin, reu.c_str()) != 0)
            ret = false;

        ret = true;
        break;
    }
    }

    return ret;

}

long int CoordinatorNode::CostFunction(int n, Position tmp, Position g_init)
{
    double distance = 0, i1 = 0, i2 = 0;
    long int cost = 0;

    i1 = sqrt(pow(g_init.x, 2) + pow(g_init.y, 2));
    i2 = sqrt(pow(tmp.x, 2) + pow(tmp.y, 2));

    distance = abs(i1 - i2);

    //mul with message?
    cost = round(distance + (penalty * abs(n)));

    return cost;

}

void CoordinatorNode::IsSucceded(const move_base_msgs::MoveBaseActionResult::ConstPtr &msg)
{

    sem_update.lock();

    if (msg->status.status == 3)
    {
        sem_iterator.lock();
        if (std::find(pos.begin(), pos.end(), g_goal) != pos.end())
        {
            pos.erase(std::find(pos.begin(), pos.end(), g_goal));
        }

        goal_delete_publisher.publish(msg->status.goal_id);
        sem_iterator.unlock();

        sem_matrix.lock();
        UpdMatrix(my_rob, atoi(g_goal.tag.c_str()), -1);
        sem_matrix.unlock();

        for (std::vector<int>::iterator it = port_others.begin(); it != port_others.end(); it++)
        {
            SendPose(g_goal, GOAL_REACHED_MSG, boost::lexical_cast<std::string>(*it));
        }

        pos_prec_mine = -1;
        fill(cost_array.begin(), cost_array.end(), std::numeric_limits<int>::max());
    }

    sem_update.unlock();
}


int CoordinatorNode::GetRandomChoice(int cont)
{

    boost::mt19937 gen(42u); // seed generator
    boost::uniform_int<> uni_dist(0, cont); // random int from -10 to 10 inclusive
    boost::variate_generator<boost::mt19937 &, boost::uniform_int<> > uni(gen, uni_dist); // callable

    return uni();
}
