/*
 * Matrix.hpp
 *
 *      Author: Nicolò Marchi
 */
 
#include <iostream>

class Matrix
{
private:
  std::vector <std::vector<int> > where;
public:
  std::vector <std::vector<int> > getWhere () const;
  int getPos (int row, int column) const;
  void setPos (int row, int column, int val);
  void resize (int row, int column);
  Matrix (int row = 0, int column = 0);
  Matrix (Matrix const & matrix);
  int sumColumn (int pos);
  int SumRow (int rob);
};


